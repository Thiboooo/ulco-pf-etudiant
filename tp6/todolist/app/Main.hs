{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

handleButton :: IO ()
handleButton = putStrLn " test"

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 300 100
    Gtk.windowSetTitle window "TODO List"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    add <- Gtk.buttonNewWithLabel("add")
    clear <- Gtk.buttonNewWithLabel("Clear")
    label <- Gtk.labelNew (Just "TODO :")

    Gtk.containerAdd window add
    Gtk.onButtonClicked add handleButton


    Gtk.widgetShowAll window
    Gtk.main


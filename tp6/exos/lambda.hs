main :: IO ()
main = do
    putStrLn (show(map (\x -> x*2) [1 .. 4]))
    putStrLn (show(map (\x -> x/2) [1 .. 4]))
    print(zipWith (\c i -> c : "-" ++ show i)  ['a'..'z'] [1..26])
    print(zipWith (\c -> \i -> c : "-" ++ show i)  ['a'..'z'] [1..26])
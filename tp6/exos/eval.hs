myTake2 :: [a] -> [a]
myTake2 = take 2 

myGet2 :: [a] -> a
myGet2 x = x !! 2

main :: IO ()
main = do
    putStrLn "Evaluation partielle"
    print(myTake2 [1..4])
    print(myGet2 [1..4])
    print(map (*2) [1..4])
    print(map (`div` 2) [1..4])
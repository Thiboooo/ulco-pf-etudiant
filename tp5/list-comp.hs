-- réimplémentations de map et filter
doubler :: [Int] -> [Int]
doubler [] = []
doubler xs = [x*2 | x<-xs]

pairs:: [Int] -> [Int]
pairs xs = [x | x<-xs, even x]

myMap :: (a -> b) -> [a] -> [b]
myMap f xs = [f x | x<-xs]

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter f xs = [x | x<-xs, f x]

multiples:: Int -> [Int]
multiples x = [n | n<-[1..x], x `mod` n == 0 ]

combinaisons :: [a] -> [b] -> [(a, b)]
combinaisons xs ys = [(x, y) | x<-xs, y<-ys]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [ (x , y , z ) | x<-[1..n],
                                  y <-[x..n],
                                  z<-[y..n],
                                  x*x + y*y == z*z]


filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) = if even x then x : filterEven1 xs else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 xs = filter even xs

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs) = if f x
    then x : myfilter f xs
    else myfilter f xs
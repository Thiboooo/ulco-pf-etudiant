import Data.Char (toUpper)

mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) =  (2*x) : mapDoubler1 xs

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 xs = map (*2) xs

mymap :: (a -> b) ->[a] -> [b]
mymap f [] = []
mymap f (x:xs) = f x : mymap f xs

main :: IO ()
main = do
    print (mapDoubler1 [1..4])
    print (mapDoubler2 [1..4])
    print (mymap (*2) [1..4::Int])
    print (mymap toUpper "foobar")
foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 (xs) = foldl (+) 0 xs
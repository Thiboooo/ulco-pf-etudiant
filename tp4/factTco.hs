fact :: Integer -> Integer
fact n = if n == 1
    then n
    else n * fact(n-1)


factTco :: Int -> Int
factTco n = aux n 0 1
    where aux 0 x y = x
    aux n x y = aux (n-1) (x+y) x

main :: IO ()
main = do
    print (fact 2)
    print (fact 5)
    print (factTco 2)
    print (factTco 5)
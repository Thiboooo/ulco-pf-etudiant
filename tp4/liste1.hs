mylength :: [a] -> Int
mylength [] = 0
mylength (_:xs) = 1 + mylength xs

toUpperString :: String -> String
toUpperString "" = ""
toUpperString (x:xs) = toUpper x : toUpperString xs


onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (x:xs) = 
    if isLetter x
        then x : onlyLetters xs
        else onlyLetters xs
import System.Environment (getArgs)

fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive(n-1) + fiboNaive(n-2)

fiboTco :: Int -> Int
fiboTco n = aux n 0 1
    where aux 0 x _ = x
          aux n x y = aux (n-1) (x+y) x

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["naive", nStr] -> print (fiboNaive (read nStr))
        ["tco", nStr] -> print (fiboTco (read nStr))
        _ -> putStrLn "usage: <naive|tco> <n>"
fact :: Integer -> Integer
fact n = if n == 1
    then n
    else n * fact(n-1)


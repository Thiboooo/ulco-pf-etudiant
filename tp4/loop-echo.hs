loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStr "> "
    text <- getLine
    if text == ""
        then return "empty line"
        else do
            putStrLn text
            loopEcho (n-1)

main :: IO ()
main = do
    result <- loopEcho 3
    putStrLn result
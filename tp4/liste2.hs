mulListe :: Int -> [Int] -> [Int]
mulListe _ [] = []
mulListe n (x:xs) = n*x : mulListe n xs

selectListe :: (Int, Int) -> [Int] -> [Int]
selectListe _ [] = []
selectListe (i0,i1) (x:xs) =
    if x >= i0 && x <= i1
        then x : selectListe (i0,i1) xs
        else selectListe (i0,i1) xs

sumListe :: [Int] -> Int
sumListe [] = 0
sumListe (x:xs) = x + sumListe xs

main :: IO ()
main = do
    print (mulListe 3 [3, 4, 1])
    print (selectListe (2, 3) [3, 4, 1])
    print (sumListe [3, 4, 1])
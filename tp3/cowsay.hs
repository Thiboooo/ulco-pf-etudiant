import System.Environment

main :: IO ()
main = do
    args <- getArgs
    let msg = unwords args
        n = length msg
    putStrLn("  " ++ replicate n '_')
    putStrLn(" <" ++ msg ++ " >")
    putStrLn("  " ++ replicate n '-')
    putStrLn "      \\   ^__^ \n\
             \       \\  (oo)\\_______ \n\
             \          (__)\\       )\\/\\   \n\ 
             \              ||----w | \n\
             \              ||     || "
palindrome :: String -> Bool
palindrome mot = if mot == reverse mot
  then True
  else False
-- palindrome :: String -> Bool
--palindrome xs = xs == reverse xs 
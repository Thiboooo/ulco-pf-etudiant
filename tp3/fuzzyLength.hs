fuzzyLength :: [a] -> String
fuzzyLength liste = if length liste == 0
  then "empty"
  else if length liste == 1 
    then "one"
    else if length liste == 2
        then "two"
        else "many"

-- fuzzyLentgh [] = "empty"
-- fuzzyLentgh [_] = "one"
-- fuzzyLentgh [_,_] = "two"
-- fuzzyLentgh _ = "many"
add2Int :: Int -> Int -> Int 
add2Int a b = a + b 

add2Double :: Double -> Double -> Double
add2Double a b = a + b

add2Num :: Num a => a -> a -> a 
add2Num a b = a + b

main :: IO ()
main = do
    print(add2Int 1 2)
    print(add2Double 1.0 2.0)
    print(add2Num 1 2.0)
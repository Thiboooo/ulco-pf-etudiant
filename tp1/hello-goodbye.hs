main :: IO ()
main = do
    putStr "What's your name: "
    texte <- getLine
    putStrLn ("Hello" ++ texte ++ "!")
    if texte == ""
        then do
            putStrLn("GoodBye !")
        else do
            main

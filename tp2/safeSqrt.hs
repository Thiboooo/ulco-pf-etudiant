import Text.Read
safeSqrt :: Double -> Maybe Double
safeSqrt n = if n<0 then Nothing else Just (sqrt n)

formatedSqrt :: Double -> String
formatedSqrt x = 
    case safeSqrt x of
        Nothing -> "la racine n'est pas possible "
        Just res -> "la racine est possible " 
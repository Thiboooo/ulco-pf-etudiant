import Text.Read (readMaybe)

main :: IO ()
main = do
    x <- getLine
    let n = readMaybe x
    print (n::Maybe Int)
    putStrLn "the end"
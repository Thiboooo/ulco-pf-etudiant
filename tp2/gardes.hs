formaterParite :: Int -> String
formaterParite n
    | even n = "pair"
    | otherwise = "impair"


formaterSigne :: Int -> String
formaterSigne n
    | n == 0 = "nul"
    | n > 0 = " positif"
    | otherwise  = "negatif"

borneEtFormate2 :: Double -> String
borneEtFormate2 x0 = show x0 ++ " -> " ++ show x1
    where x1 | x0 < 0 = 0
             | x0 > 1 = 1
             | otherwise = x0
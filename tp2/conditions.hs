formaterParite :: Int -> String
formaterParite n = if even n
  then "pair"
  else "impair"


formaterSigne :: Int -> String
formaterSigne n = if n == 0
  then "nul"
  else if n > 0
    then "positif"
    else "negatif"

borneEtFormate1 :: Double -> String
borneEtFormate1 x0 = show x0 ++ " -> " ++ show x1
    where x1 = if x0 < 0
              then 0
              else if x0 > 1
                    then 1
                    else x0
formateNul1 :: Int -> String
formateNul1 n = show n ++ " est " ++ fmt n
    where fmt n = case n of 
            0 -> "nul"
            _ -> "non nul"

formateNul2 :: Int -> String
formateNul2 n = show n ++ " est " ++ fmt n
    where fmt 0 = " nul"
          fmt _ = "non nul"
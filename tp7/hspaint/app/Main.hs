{-# LANGUAGE OverloadedStrings #-}

import           GI.Cairo.Render
import           GI.Cairo.Render.Connector (renderWithContext)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.Gtk.Objects.Widget
import           Data.IORef

type Point = ( Double , Double )

point :: IORef Point -> IO()
point ref = writeIORef ref (20,20)

type Pointt = ( Double , Double )

pointt :: IORef Point -> IO()
pointt ref = writeIORef ref (40,40)

handleDraw :: Gtk.DrawingArea -> Render Bool
handleDraw canvas = do
    --p1 <- liftIO(newIORef (0,0))
    --point p1
    --xyp1 <- liftIO(readFile p1)
    --print(xyp1)
    --p2 <- liftIO(newIORef (0,0))
    --pointt p2
    --xyp2 <- liftIO(readFile p2)
    --print(xyp2)
    moveTo 0 0
    lineTo 200 100
    stroke
    return True

handlePress :: Gdk.EventButton -> IO Bool
handlePress b = do
    -- x <- b.eventButton_x
    -- y <- b.eventButton_y
    -- x <- Gdk.getEventButtonX b
    -- moveTo x y
    -- lineTo x+0.01 y+0.01
    -- l'astuce ici serrait de faire une ligne ( une ligne si petite quelle remplace le point ) a chaque fois que la souris 
    Gtk.mainQuit
    return True

main :: IO ()
main = do
    --p1 <- newIORef (0,0)
    --point p1
    --xyp1 <- readFile p1
    --print(xyp1)
    --p2 <- newIORef (0,0)
    --pointt p2
    --xyp2 <- readFile p2
    --print(xyp2)
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    canvas <- Gtk.drawingAreaNew
    _ <- Gtk.onWidgetDraw canvas $ renderWithContext $ handleDraw canvas
    Gtk.containerAdd window canvas 

    Gtk.widgetSetEvents canvas [ Gdk.EventMaskButtonPressMask ]
    _ <- onWidgetButtonPressEvent canvas handlePress 

    Gtk.widgetShowAll window
    Gtk.main


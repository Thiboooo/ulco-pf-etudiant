import System.Random

-- fonction qui renvoie un dessin de pendu selon un numero
erreur :: Int -> IO ()
erreur e = do
    let liste_pendu = ["    _______   \n\\   |/          \n\\   |           \n\\   |           \n\\   |           \n\\   |           \n\\___|___________","    _______   \n\\   |/      |   \n\\   |      (_)  \n\\   |           \n\\   |           \n\\   |           \n\\___|___________","    _______   \n\\   |/      |   \n\\   |      (_)  \n\\   |      L|/  \n\\   |       |   \n\\   |      / L  \n\\___|___________"]
    putStrLn(liste_pendu !! e)

-- fonction qui recherche une lettre dans un mot et qui renvoie un bool 
recherche :: [Char] -> Char -> Bool
recherche [] _ = False
recherche (lettre:mot) res = if (lettre == res)
        then True
        else recherche mot res

--chartostring fonction qui transforme un char en string      
charToString :: Char -> String
charToString c = [c]

--cachemot fonction qui remplace les lettres manquantes par des x
cachermot :: [Char] -> Char -> [Char]
cachermot [] _ = []
cachermot (lettre:mot) res =
    if recherche res lettre 
        then 

main :: IO()
main = do
    let liste = ["function","functional programming","haskell","list","patter matching","recursion","tuple","type system"]
    nb <- randomRIO(0,7) :: IO Int
    let alea = liste !! nb
    putStrLn alea
    putStrLn "entrez une lettre :"
    lettre <- getLine
    let compteur = 0
    if (recherche charToString(alea) lettre == False)
        then 
            erreur compteur 
            let temp = compteur 
            let compteur = temp + 1
        else
            putStrLn"lettre trouver "
    
    
    


